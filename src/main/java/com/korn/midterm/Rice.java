package com.korn.midterm;

import java.util.Scanner;

public class Rice {
    private String name;
    private int price;
    private static int change = 0;
    private static int pay = 0;

    public Rice(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public void print() {
        System.out.println(name + " " + "price" + " " + price + " " + "Bath");
    }

    public static void friedriceprice() {
        int price = 50;
        Scanner sc = new Scanner(System.in);
        System.out.print("pay: ");
        pay = sc.nextInt();
        if (pay >= price) {
            change = pay - price;
            System.out.println("change: " + change);
        } else {
            System.out.println("Your balance is not enough");
            System.out.println("please pay again");
            System.out.print("pay: ");
            pay = sc.nextInt();
            change = pay - price;
            System.out.println("change: " + change);
        }
    }

    public static void seafoodprice() {
        int price = 70;
        Scanner sc = new Scanner(System.in);
        System.out.print("pay: ");
        pay = sc.nextInt();
        if (pay >= price) {
            change = pay - price;
            System.out.println("change: " + change);
        } else {
            System.out.println("Your balance is not enough");
            System.out.println("please pay again");
            System.out.print("pay: ");
            pay = sc.nextInt();
            change = pay - price;
            System.out.println("change: " + change);
        }
    }

    public static void porkprice() {
        int price = 100;
        Scanner sc = new Scanner(System.in);
        System.out.print("pay: ");
        pay = sc.nextInt();
        if (pay >= price) {
            change = pay - price;
            System.out.println("change: " + change);
        } else {
            System.out.println("Your balance is not enough");
            System.out.println("please pay again");
            System.out.print("pay: ");
            pay = sc.nextInt();
            change = pay - price;
            System.out.println("change: " + change);
        }
    }
}
