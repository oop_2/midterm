package com.korn.midterm;

import java.util.Scanner;

public class Choice {
    public static void main(String[] args) {
        int choice = 0;
        while (true) {
            printmenu();
            choice = inputchoice();
            if (choice == 1) {
                Rice.friedriceprice();
                break;
            }
            if (choice == 2) {
                Rice.seafoodprice();
                break;
            }
            if (choice == 3) {
                Rice.porkprice();
                break;
            }
        }
    }

    public static void printmenu() {
        System.out.println("---------Menu----------");
        Rice friedrice = new Rice("friedrice", 50);
        Rice seafood = new Rice("seafood", 70);
        Rice pork = new Rice("pork", 100);
        friedrice.print();
        seafood.print();
        pork.print();
        System.out.println("-----------------------");
    }

    public static int inputchoice() {
        int choice;
        while (true) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please input your choice(1-3): ");
            choice = sc.nextInt();
            if (choice >= 1 && choice <= 3) {
                return choice;
            }
            System.out.println("Error: Please input between 1-3");
        }
    }
}
